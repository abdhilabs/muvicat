//
//  CastModel.swift
//  MuviCat
//
//  Created by Abdhi on 25/06/21.
//

import Foundation

struct CastModel: Equatable, Identifiable {
    
    let id: Int
    let name: String
    let profilePath: String
}

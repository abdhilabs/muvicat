//
//  UIColor+Ext.swift
//  MuviCat
//
//  Created by Abdhi on 18/06/21.
//

import UIKit

extension UIColor {
    
    static var tabIconColor: UIColor { return "FFD130".hexToUIColor() }
    static var tabColor    : UIColor { return "202123".hexToUIColor() }
    static var bgColor     : UIColor { return "25272A".hexToUIColor() }
    static var iconFavColor: UIColor { return "FFCC00".hexToUIColor() }
}
